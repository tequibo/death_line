﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectorButton : MonoBehaviour
{
    public int index;

    private LevelSelectorManager manager;
    private Text text;
    private Button button;

    public void Init(int _index, LevelSelectorManager _manager)
    {
        index = _index;
        manager = _manager;

        text = transform.GetComponentInChildren<Text>();
        text.text = (index + 1).ToString();

        button = transform.GetComponentInChildren<Button>();
        button.onClick.AddListener(SelectLevel);
    }

    private void SelectLevel()
    {
        Debug.Log(index);
        manager.game.levelManager.currentLevelNum = index - 1;
        manager.game.NextLevel();
        manager.CloseLevelSelectorPanel();
    }
}
