﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LevelSelectorManager : MonoBehaviour
{
    public Game game;
    public GameObject levelSelectorPanel;
    private List<LevelSelectorButton> levelSelectorButtons;

    private void Start()
    {
        //Init();
        CloseLevelSelectorPanel();
    }


    public void Init()
    {
        levelSelectorButtons = transform.GetComponentsInChildren<LevelSelectorButton>().ToList();
        for (int i = 0; i < levelSelectorButtons.Count; i++)
        {
            levelSelectorButtons[i].Init(i, this);
        }

    }

    public void OpenLevelSelectorPanel()
    {
        levelSelectorPanel.SetActive(true);
        Init();
    }

    public void CloseLevelSelectorPanel()
    {
        levelSelectorPanel.SetActive(false);
    }
}
