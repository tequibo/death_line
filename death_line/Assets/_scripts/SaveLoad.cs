﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveLoad
{

	public static GameState savedGame = new GameState();

	public static void Save()
	{

		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/savedGames.gd"); 
		bf.Serialize(file, SaveLoad.savedGame);
		file.Close();

		Debug.Log ("Game saved");
	}

	public static void Load()
	{
		if (File.Exists(Application.persistentDataPath + "/savedGames.gd"))
		{

			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
			SaveLoad.savedGame = (GameState)bf.Deserialize(file);
			file.Close();
		}
		else
		{
			//			Debug.Log ("THERE IS NO FILE!");
		}
	}

	public static void ResetSave()
	{
		savedGame = new GameState();
		Debug.Log ("Saves reseted");
	}
}


[System.Serializable]
public class GameState
{

	public int collected;
	public int lastScore;
	public int bestScore;
	public int playCount;
	public int giftsRecieved;
    public int currentLevelNum;
    public int currentPaletteNum;

	public bool soundOn; 
	public int isNoAds; 
	public bool isTutorialOn;


	public GameState()
	{
		collected = 0;
		lastScore = 0;
		playCount = 0;
		bestScore = 0;
		giftsRecieved = 0;
        currentLevelNum = 0;
        currentPaletteNum = 0;

		soundOn = true;
		isNoAds = 0;
		isTutorialOn = true;

	}
		
}


