﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineManager : MonoBehaviour
{

    // public Game game;

    public float distanceBetweenPoints = 0.05f;
    public float smoothSegmentSize = 0.05f;
    public bool dontMoveForDebug;
    public bool dontSmoothForDebug;
    [Space]
    public GameObject linePrefab;

    private LineController currentLine;
    private LineController movingLine;

    public bool isInputForbidden;

    public Transform startLinePositionTransform;
    private Vector3 diffLinePosition;

    private void Start() {
        //startLinePosition = Vector3.up * -5.6f;
        //Time.timeScale = 0.1f;
    }
    public void CleanLines(){
        int childs = transform.childCount;
        for (int i = childs - 1; i >= 0; i--) {
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
    }
    private void LateUpdate() {
        if (Input.GetMouseButtonDown(0) && !isInputForbidden) {
            GameObject go = Instantiate(linePrefab);
            go.transform.SetParent(transform);
            go.transform.position=new Vector3(-500,0,0);
            currentLine = go.GetComponent<LineController>();
            //if (movingLine!=null) Destroy(movingLine.gameObject);
            // diffLinePosition = startLinePositionTransform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
            diffLinePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            // game.character.AddRbToLineJoint( currentLine.head.GetComponent<Rigidbody2D>());
        }
        if (currentLine != null) {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);// + diffLinePosition;
            RaycastHit hit; 
            int layerMask = 1 << 8;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); 
            if (Physics.Raycast (ray, out hit, 100.0f, layerMask)) {
                // currentLine.UpdateLine(new Vector3(hit.point.x, hit.point.y, hit.point.z));
                //currentLine.UpdateLine(hit.point+hit.normal*1.5f);          
                currentLine.UpdateLine(hit.point);          
            }
            else{
                if (currentLine != null && currentLine.LineLength() < 0.1f) {
                    Destroy(currentLine.gameObject);
                    return;
                }
                StartMovingCurrentLine();
            }
            // currentLine.UpdateLine(new Vector3(mousePos.x, mousePos.y, 30f));
        }
        if (Input.GetMouseButtonUp(0)) {
            if (currentLine != null && currentLine.LineLength() < 0.1f) {
                Destroy(currentLine.gameObject);
                return;
            }
            StartMovingCurrentLine();
        }

        

        // if (movingLine != null) {
        //     movingLine.MoveLine();
        // }
    }

    public void StartMovingCurrentLine() {
        if (currentLine == null) return;

        movingLine = currentLine;
        movingLine.CreateMovablePoints();
        currentLine = null;

    }
}
