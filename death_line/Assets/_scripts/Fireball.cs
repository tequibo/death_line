﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
     private void OnCollisionEnter(Collision other) {
        if(other.transform.CompareTag("OBSTACLE")){
           GetComponent<LineController>().DestroyLine();
        }
    }
}
