using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using GameAnalyticsSDK;

public class LevelManager:MonoBehaviour {
    public int currentLevelNum = 0;
    Game game;
    //public List<LevelSet> levels;
    public List<GameObject> levels = new List<GameObject>();
    GameObject currentLevel;
    public LevelDataSO[] levelColors;
    public int colorChangePerLevels=5;
    public bool forceFirstPalette = false;
    int currentPaletteNum = 0;
    int currentPaletteCount = 0;
    Vector3 cameraStartPosition;
    Vector3 cameraSwitchPosition;
    public Animator blackoutAnimator;
    public int enemiesOnLevel=0;
    public GameObject fog;
    public bool resetProgress=false;

    public IEnumerator LoadLevel(int levelNum){
        blackoutAnimator.Play("BLACKOUT");
        GameObject newLevel = Instantiate(levels[levelNum%levels.Count]);

        enemiesOnLevel = newLevel.GetComponentsInChildren<Enemy>().Length;
        LineController[] lines = FindObjectsOfType<LineController>();
           for (int i = 0; i < lines.Length; i++) {
               LineController l = lines[i];
               l.DestroyLine();
        } 
        yield return new WaitForSeconds(.33f);
        if(currentLevel!=null){
            Debug.Log(currentLevel);
            Destroy(currentLevel);
            LineManager lm = FindObjectOfType<LineManager>();
            if(lm!=null){
                lm.CleanLines();
            }
            // LeanTween.move(currentLevel,new Vector3(-35f,0,0),.75f).setEaseOutElastic();
        }
        currentLevel = newLevel;//Instantiate(levels[levelNum%levels.Count]);
        currentLevel.SetActive(true);
        currentPaletteCount++;
        if(currentPaletteCount>colorChangePerLevels-1){
            currentPaletteCount=0;
            currentPaletteNum++;
        }
        if(currentPaletteNum>levelColors.Length-1){
            currentPaletteNum=0;
        }
        //set materials
        LevelDataSO settings = levelColors[currentPaletteNum];
        GameObject[] obstacles = GameObject.FindGameObjectsWithTag("OBSTACLE");
        GameObject[] grounds = GameObject.FindGameObjectsWithTag("GROUND");
        for (int i = 0; i < obstacles.Length; i++){
            MeshRenderer mr = obstacles[i].GetComponent<MeshRenderer>();
            if(mr!=null){
                mr.material = Random.Range(0,1f)>.5f ? settings.obstacle:settings.obstacleOdd;
            }
        }
        for (int i = 0; i < grounds.Length; i++){
            MeshRenderer mr = grounds[i].GetComponent<MeshRenderer>();
            if(mr!=null){
                mr.material = settings.ground;
            }
        }
        MeshRenderer fogMr = fog.GetComponent<MeshRenderer>();
            if(fogMr!=null){
                fogMr.material = settings.fog;
            }
            
        Enemy[] enemies = currentLevel.GetComponentsInChildren<Enemy>();
        foreach (Enemy enemy in enemies) {
            enemy.OnDeath.AddListener(game.EnemyKilled);
        }
        //return enemies.Length;
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, Application.version, currentLevelNum.ToString("00000"));
    }
    private void Awake() {
        Application.targetFrameRate = 60;
        if(resetProgress){
            SaveLoad.savedGame.currentLevelNum=0;
            SaveLoad.savedGame.currentPaletteNum=0;
            SaveLoad.Save();
        }
        cameraStartPosition = Camera.main.transform.position;
        SaveLoad.Load();
        currentLevelNum = SaveLoad.savedGame.currentLevelNum;
        currentPaletteNum = SaveLoad.savedGame.currentPaletteNum;
        game = FindObjectOfType<Game>();
        for (int i = 0; i < levels.Count; i++)
        {
            levels[i].SetActive(false);
        }
    }
    private void Start()
    {
        GameAnalytics.Initialize();
    }

    public int RestartLevel(){
        StartCoroutine(LoadLevel(currentLevelNum));
        return enemiesOnLevel;
    }

    public int NextLevel(){
        currentLevelNum++;
        SaveLoad.savedGame.currentLevelNum = currentLevelNum;
        SaveLoad.Save();
        StartCoroutine(LoadLevel(currentLevelNum));
        return enemiesOnLevel;
    }
}
[System.Serializable] 
public class LevelSet{
    public List<GameObject> list;
}

public static class IListExtensions {
    /// <summary>
    /// Shuffles the element order of the specified list.
    /// </summary>
    public static void Shuffle<T>(this IList<T> ts) {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i) {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }
}