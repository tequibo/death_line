using UnityEngine;

[CreateAssetMenu(fileName = "LevelData", menuName = "ScriptableObjects/CreateLevelData", order = 1)]
public class LevelDataSO : ScriptableObject
{
    public Material obstacle;
    public Material obstacleOdd;
    public Material ground;
    public Material fog;
}

