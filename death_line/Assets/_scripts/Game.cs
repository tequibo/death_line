﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using GameAnalyticsSDK;

public class Game:MonoBehaviour {
    public UnityEvent OnWin;
    public UnityEvent OnLose;
    public int enemiesAlive = 0;
    public LevelManager levelManager;
    UIManager uiManager;
    LineManager lineManager;
    private void Awake() {
        uiManager = FindObjectOfType<UIManager>();
        levelManager = FindObjectOfType<LevelManager>();
        lineManager = FindObjectOfType<LineManager>();
    }
    void Start(){        
        enemiesAlive = levelManager.RestartLevel();
        SaveLoad.Load();
        uiManager.levelNumber.text = "LEVEL "+(SaveLoad.savedGame.currentLevelNum+1);
        uiManager.SetEnemiesCount(enemiesAlive);
        uiManager.ShowTutorial();
    }
    public void EnemyKilled(){        
        enemiesAlive--;
        uiManager.SetEnemiesCount(enemiesAlive);
        if(enemiesAlive<=0){
            LeanTween.delayedCall(1f,Win);
            uiManager.Fireworks();
            //Win();
        }
    }
   
    void Win(){
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, Application.version, levelManager.currentLevelNum.ToString("00000"));
        uiManager.HideHUD();
        uiManager.ShowWinScreen();
        lineManager.isInputForbidden = true;
    }
    public void NextLevel(){
        uiManager.HideWinScreen();
        enemiesAlive = levelManager.NextLevel();
        uiManager.SetEnemiesCount(enemiesAlive);
        uiManager.levelNumber.text = "LEVEL "+(levelManager.currentLevelNum+1);
        lineManager.isInputForbidden = false;
    }
    public void RestartLevel(){
        enemiesAlive = levelManager.RestartLevel();
        uiManager.SetEnemiesCount(enemiesAlive);
    }
    public void Test(){
        Debug.Log("called next level");

    }
}





