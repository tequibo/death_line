﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollider : MonoBehaviour
{
    private void OnCollisionEnter(Collision other) {
        
        if(!other.transform.CompareTag("GROUND") && !other.transform.CompareTag("ENEMY") && other.relativeVelocity.magnitude>2f){
            SendMessageUpwards("Damage", 1);
            
           Debug.Log("enemy sub hit "+other.relativeVelocity.magnitude+ " " +other.gameObject.name);
        }
    }
    public void Hit(){
        SendMessageUpwards("Damage", 1);
    }
}
