﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(LineRenderer))]
public class LineController : MonoBehaviour
{

    private bool dontMoveForDebug;
    private bool dontSmoothForDebug;
    private float distanceBetweenPoints = 0.05f;
    private float smoothSegmentSize = 0.05f;
    private LineManager manager;
    [Space]
    public List<Vector3> points;
    public List<Vector3> movablePoints = new List<Vector3>();
    public LineRenderer lineRenderer;
    public TrailRenderer trailRenderer;
    public ParticleSystem drawingParticles;
    public ParticleSystem liveParticles;
    public ParticleSystem explosionParticles;
    public Transform head;
    public float radius=1f;
    public float power=100f;
    private bool alive=true;
    private Vector3 oldPosition;
    private Vector3 velocity;
    private float hp=1;
    public float recieveDamage=.15f;
    public bool showLineRenderer = true;
    public bool showLineTrail = false;
    private void Start()
    {
        manager = FindObjectOfType<LineManager>();
        dontMoveForDebug = manager.dontMoveForDebug;
        dontSmoothForDebug = manager.dontSmoothForDebug;
        distanceBetweenPoints = manager.distanceBetweenPoints;
        smoothSegmentSize = manager.smoothSegmentSize;
        //HeadOff();
        liveParticles.Stop();
        explosionParticles.Stop();
        lineRenderer.enabled=showLineRenderer;

    }
    private void Awake() {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.enabled=false;
        oldPosition = transform.position;
        
    }
    private void Update() {
        if(movablePoints.Count>=2){
            MoveLine();
        }
    }
    public void MoveLine()
    {
        if(dontMoveForDebug) return;
        if(movablePoints.Count < 2) {
            DestroyLine();
            return;
        }
        if(alive){
            Vector3 newPoint = movablePoints.Last() + (movablePoints[1] - movablePoints[0]);
            for (int i = 1; i < movablePoints.Count; i++)            {
                movablePoints[i - 1] = movablePoints[i];
            }
            movablePoints[movablePoints.Count - 1] = newPoint;
        }
        if(head.position.x<-25||head.position.x>25||head.position.z<-25||head.position.z>55) DestroyLine();
        UpdateLineRenderer(movablePoints);
    }


    public void UpdateLine(Vector3 point)
    {
        if (points == null || points.Count == 0) {
            points = new List<Vector3>();
            SetPoint(point);
            return;
        }

        if (Vector3.Distance(point, points.Last()) > distanceBetweenPoints)
        {
            SetPoint(point);
        }
    }

    private void SetPoint(Vector3 point)
    {
        points.Add(point);
        UpdateLineRenderer(points);
    }

    private void UpdateLineRenderer(List<Vector3> myPoints)
    {
            // https://forum.unity.com/threads/easy-curved-line-renderer-free-utility.391219/
            Vector3[] smoothedPoints = dontSmoothForDebug ? myPoints.ToArray() : LineSmoother.SmoothLine(myPoints.ToArray(), smoothSegmentSize);

            lineRenderer.positionCount = smoothedPoints.Length;
            lineRenderer.SetPositions(smoothedPoints);

            transform.position = smoothedPoints.Last();

    }
    
    public void CreateMovablePoints()
    {
        UIManager ui =  FindObjectOfType<UIManager>();
        if(ui!=null){
            ui.HideTutorial();
        }
        movablePoints = points;
        HeadOn();
        drawingParticles.Stop();
        liveParticles.Play();  
    }

    public void HeadOn()
    {
        head.GetComponent<MeshRenderer>().enabled = true;
    }

    public void HeadOff()
    {
        head.GetComponent<MeshRenderer>().enabled = false;
    }


    public float LineLength()
    {
        float length = 0;
        for (int i = 0; i < points.Count-1; i++)
        {
            length += Vector3.Distance(points[i], points[i + 1]);
        }
        return length;
    }
    // private void OnDrawGizmos() {
    //    // Debug.Log(velocity);
    //     Gizmos.DrawRay(transform.position,velocity);
    // }
    private void FixedUpdate() {
        velocity = transform.position - oldPosition;
        oldPosition=transform.position;
    }
    void Damage(float amount){
        hp-=amount;
        if(hp<=0)
            DestroyLine();
    }
    public void DestroyLine(){
        if(alive){
            trailRenderer.transform.SetParent(transform.parent);
            Vector3 explosionPos = transform.position;
            Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
            foreach (Collider hit in colliders)
            {
                Rigidbody rb = hit.GetComponent<Rigidbody>();
                if (rb != null){
                    //Debug.Log(hit);
                    rb.AddExplosionForce(power, explosionPos, radius, 3.0F);
                }
                EnemyCollider e = hit.GetComponent<EnemyCollider>();
                if(e!=null){
                    e.Hit();
                }
            }

            HeadOff();
            
            liveParticles.Stop();  
            explosionParticles.Play();
            explosionParticles.transform.SetParent(transform.parent);
            alive=false;
            LeanTween.value(gameObject,0.75f,0,.5f).setOnUpdate( (float val)=>{
                lineRenderer.startWidth=0;
                lineRenderer.endWidth=val;
            }).setOnComplete(()=>{Destroy(gameObject);});;     
        }   
    }
    private void OnCollisionEnter(Collision other) {
        Debug.Log(velocity+ " collision "+other.gameObject.name);
        if(other.rigidbody!=null){
            other.rigidbody.AddForce(velocity*power, ForceMode.Impulse);
            //explosionParticles.Play();
            //explosionParticles.transform.SetParent(transform.parent);
            Damage(recieveDamage);
        }
        EnemyCollider e = other.gameObject.GetComponent<EnemyCollider>();
            if(e!=null){
                e.Hit();
               // DestroyLine();
            }

        
        // if(other.transform.CompareTag("OBSTACLE") || other.transform.CompareTag("ENEMY") || other.transform.CompareTag("OBJECT")){
        if(other.transform.CompareTag("OBSTACLE") ){
           DestroyLine();
           
        }
    }
}
