﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Lean.Gui;
public class Bomb : MonoBehaviour
{
    public UnityEvent Explosion;
    public GameObject explosionPrefab;
    public Transform mesh;
    public bool alive=true;
    public float damageDelay=.6f;
    public float radius=3f;
    public float power=10f;
    public ParticleSystem fuse;
    private void Start() {
        fuse.Stop();
    }
    private void OnCollisionEnter(Collision other) {     
        Damage();
        if(other.transform.CompareTag("LINE")){
            
        }
    }
    private void Update() {
        transform.Rotate(Vector3.up,Time.deltaTime*11f);
    }
    public void Damage() {
        if(alive){
            alive=false;
            StartCoroutine(Explode());
        }
    }
    IEnumerator Explode(){
        fuse.Play();
        GetComponent<Animator>().Play("BLINK");
        yield return new WaitForSeconds(damageDelay);
        Camera.main.GetComponent<LeanShake>().Strength=1;
        iOSHapticFeedback.Instance.Trigger(iOSHapticFeedback.iOSFeedbackType.ImpactHeavy);
        GameObject go = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        go.transform.SetParent(this.transform);
        mesh.gameObject.SetActive(false);

        // Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);
        // foreach (var hitCollider in hitColliders)
        // {
        //     Enemy e = hitCollider.GetComponent<Enemy>();
        //     if(e!=null){
        //         e.Damage(9999);
        //     }
        // }

        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if (rb != null){
                //Debug.Log(hit);
                rb.AddExplosionForce(power, explosionPos, radius, 3.0F);
            }
        }
    

        // var enemies = FindObjectsOfType<Enemy>();
        // foreach (var e in enemies)
        // {
        //     e.Damage(9999);
        // }
    }
    
}
