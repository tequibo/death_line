using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour {
    public Animator winScreen;
    public Animator failScreen;
    public Animator tutorial;
    public Animator scoreAnimator;
    public TextMeshProUGUI enemyCount;
    public TextMeshProUGUI levelNumber;
    public ParticleSystem confetti;
    private void Start() {
        confetti.Stop();
    }
    public void ShowTutorial(){
        tutorial.gameObject.SetActive(true);
        tutorial.Play("SHOW");
    }
    public void HideTutorial(){
        tutorial.Play("HIDE");
    }
    public void Fireworks(){
        confetti.gameObject.SetActive(true);
        confetti.Play();
    }
    public void ShowWinScreen(){        
        //failScreen.Play("HIDE");
        winScreen.gameObject.SetActive(true);
        winScreen.Play("SHOW");

    } 
    public void HideWinScreen(){
        confetti.Stop();
        confetti.gameObject.SetActive(false);
        
        //failScreen.Play("HIDE");
        winScreen.Play("HIDE");

    } 
    public void ShowFailScreen(){
        failScreen.Play("SHOW");
        winScreen.Play("HIDE");
    } 
    public void ShowHUD(){

    }
    public void HideHUD(){

    }
    public void SetEnemiesCount(int num){
        scoreAnimator.Play("SCORE");
        enemyCount.text = num.ToString();
    }
}