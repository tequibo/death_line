using System.Collections;
using System.Collections.Generic;
using Lean.Gui;
using UnityEngine;
using UnityEngine.Events;

public class Enemy:MonoBehaviour {
    public UnityEvent OnDeath;
    public ConfigurableJoint torso;
    public int hp=1;
    bool alive=true;
    public GameObject eyes;
    // public Rigidbody rigidbody;
    private void OnCollisionEnter(Collision other) {
        
        if(!other.transform.CompareTag("GROUND") && !other.transform.CompareTag("ENEMY") && other.relativeVelocity.magnitude>2f){

            Damage(1);
        //    Debug.Log("enemy hit " +other.gameObject.name);
        }
    }
    private void OnTriggerEnter(Collider other) {
       if(other.transform.CompareTag("LINE")){
            //Damage(1);
        }
    }
    private void Update() {
        if((transform.position.y<-10||transform.position.y>30)&&alive){
            Damage(999);
            Camera.main.GetComponent<LeanShake>().Strength=.2f;

        }
    }
    public void Damage(int amount){
        hp-=amount;
        if(hp<=0 && alive){
            eyes.SetActive(false);
            alive=false;
            OnDeath.Invoke();
            JointDrive jdrive = new JointDrive();
            jdrive.positionDamper = 50f;
            torso.angularXDrive = jdrive;
            torso.angularYZDrive = jdrive;
            ConfigurableJoint[] joints = GetComponentsInChildren<ConfigurableJoint>();
            for (int i = 0; i < joints.Length; i++) {
                ConfigurableJoint joint = joints[i];
                joint.angularXDrive = jdrive;
                joint.angularYZDrive = jdrive;
            }
            //GameObject go = Instantiate(deathBody,transform.position,Quaternion.identity);
            //go.transform.SetParent(transform.parent);
            // rigidbody.awake();
            //this.gameObject.SetActive(false);
        }
    }
}