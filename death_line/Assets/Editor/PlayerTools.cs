﻿using UnityEditor;
using UnityEngine;

public class PlayerTools
{
    [MenuItem("Tools/Save and Load/Reset save")]
    static void ResetSaveCall()
    {
        SaveLoad.ResetSave();
        SaveLoad.Save();
    }

    //[MenuItem("Tools/Save and Load/Load Test Save")]
    //static void LoadTestSaveCall()
    //{
    //    SaveLoad.LoadTestSave();
    //    SaveLoad.Save();
    //}
}